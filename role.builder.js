var collectRoutine = require('routine.collect.energy');
var builder = require('routine.builder');
var upgrader = require('routine.upgrade');

var roleBuilder = {
    run: function(creep) {
        if (creep.memory.building && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.building = false;
        }

        if (!creep.memory.building && creep.store.getFreeCapacity() == 0) {
            creep.memory.building = true;
        }

        if (creep.memory.goingHome && creep.room.name == creep.memory.homeRoom) {
            creep.memory.goingHome = false;
        }

        if (creep.memory.goingHome) {
                const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
                const exit = creep.pos.findClosestByRange(exitDir);
                creep.moveTo(exit);
        } else if (creep.memory.building) {
            var constructionSites = []
            for (var target in Game.constructionSites) {
                var site = Game.getObjectById(target);
                constructionSites.push(site);
            }

            // TODO: Store in memory
            if (constructionSites.length > 0) {
                if (creep.build(site) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(site);
                }
            } else {
                // Nothing to build... TODO: rework this to be better.
                if (creep.room.controller) {
                    if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(creep.room.controller);
                    }
                }
            }
            //var targets = builder.hasConstructionSites(creep);
            //if (targets) {
            //    builder.goBuild(creep, targets[0]);
            //} else {
            //    upgrader.goUpgrade(creep);
            //}
        } else {
            if (creep.room.energyAvailable < creep.store.getCapacity()) {
                const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
                const exit = creep.pos.findClosestByRange(exitDir);
                creep.moveTo(exit);
            } else {
                var containers = creep.room.find(FIND_STRUCTURES, {
                    filter: (s) => {
                        return (s.structureType == STRUCTURE_CONTAINER) &&
                            s.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
                    }
                });

                if (containers.length > 0) {
                    if (creep.withdraw(containers[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(containers[0]);
                    }
                } else {
                    if (creep.room.storage && creep.room.storage.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                        if (creep.withdraw(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(creep.room.storage);
                        }
                    } else {
                        if (creep.room.name == creep.memory.homeRoom) {
                            var source = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
                            if (source) {
                                if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                                    creep.moveTo(source);
                                }
                            } else {
                                console.log(creep.room.name + ": Builder is bored; Waiting for resources.");
                            }
                        } else {
                            creep.memory.goingHome = true;
                            console.log("Builder unable to find resources to use for jobs...");
                            console.log("going home.");
                        }
                    }
                }
            }
        }
    }
};

module.exports = roleBuilder;
