var collectRoutine = require('routine.collect.energy');
var engineer = require('routine.engineer');
var builder = require('routine.builder');

var roleEngineer = {
    run: function(creep) {
        if (creep.room.name == creep.memory.homeRoom) {
            engineer.checkStatus(creep);
            if (creep.memory.repairing) {
                engineer.goRepair(creep);
            } else {
                if (creep.room.storage && creep.room.storage.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
                    collectRoutine.fromStorage(creep);
                } else {
                    var containers = creep.room.find(FIND_STRUCTURES, {
                        filter: (s) => s.structureType == STRUCTURE_CONTAINER &&
                        s.store.getUsedCapacity(RESOURCE_ENERGY) > 0
                    });

                    if (containers.length > 0) {
                        if (creep.withdraw(containers[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(containers[0]);
                        }
                    } else {
                        var source = creep.pos.findClosestByRange(FIND_SOURCES);
                        if (source) {
                            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                                creep.moveTo(source);
                            }
                        } else {
                            console.log(creep.room.name + " No resources to collect for repair jobs");
                        }
                    }
                }
            }
        } else {
            const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
            const exit = creep.pos.findClosestByRange(exitDir);
            creep.moveTo(exit);
        }
    }
}

module.exports = roleEngineer;
