var repair = {
    findRepairTargets: function(entity) {
        const targets = entity.room.find(FIND_STRUCTURES, {
            filter: structure => structure.hits < structure.hitsMax
        });

        targets.sort((a,b) => a.hits - b.hits);

        return targets;
    }
}

module.exports = repair;
