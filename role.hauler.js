var roleHauler = {
    run: function(creep) {
        if (creep.memory.hauling && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.hauling = false;
        }

        if (!creep.memory.hauling && creep.store.getUsedCapacity() != 0) {
            creep.memory.hauling = true;
        }

        if (creep.memory.linkId == undefined) {
            // TODO: Implement room name check
            creep.memory.linkId = "5de079267109a94cf69cf48b";
        }
        if (creep.memory.upgradeContainerId == undefined) {
            // TODO: Implement room name check
            creep.memory.upgradeContainerId = "5de3446ebe319b8a867c5839";
        }

        if (creep.memory.hauling) {
            // Populate Link by storage
            const link = Game.getObjectById(creep.memory.linkId);
            if (link.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
                if (creep.transfer(link, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(link);
                }
            } else {
                // Refill energy
                // Haul to upgradeContainer
                const upgradeContainer = Game.getObjectById(creep.memory.upgradeContainerId);

                if (creep.transfer(upgradeContainer, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(upgradeContainer);
                }
            }
        } else {
            // Refill energy
            if (creep.withdraw(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.storage);
            }
        }
    }
}


module.exports = roleHauler;
