var roleRaider = {
    run: function(creep) {
        if (creep.memory.inRaidRoom) {
            if (creep.store.getFreeCapacity() > 0) {
                harvestSources(creep);
            } else {
                goHome(creep);
            }
        } else {
            if (creep.memory.inHome) {
                if (creep.store.getUsedCapacity() == 0) {
                    goRaidRoom(creep);
                } else {
                    goDeposit(creep);
                }
            }
        }
    }
}

function goDeposit(creep) {
    const storage = creep.room.storage;
    if (creep.transfer(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
        creep.moveTo(storage);
    }
}
function harvestSources(creep) {
    var target = creep.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
    if (target) {
        if (creep.harvest(target) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }
    }
}

function goRaidRoom(creep) {
    const raidPos = new RoomPosition(37, 47, "E33S37");
    if (creep.pos.isEqualTo(raidPos)) {
        creep.memory.inRaidRoom = true;
        creep.memory.inHome = false;
    } else {
        creep.moveTo(raidPos);
    }
}

function goHome(creep) {
    const homePos = new RoomPosition(37, 1, "E33S38");
    if (creep.pos.isEqualTo(homePos)) {
        creep.memory.inRaidRoom = false;
        creep.memory.inHome = true;
    } else {
        creep.moveTo(homePos);
    }
}

function isHome(creep) {
    return creep.room.name == "E33S38";
}

module.exports = roleRaider;
