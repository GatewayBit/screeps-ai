var engineer = {
    checkStatus: function(creep) {
        checkRepairList(creep);

        if (creep.memory.repairing && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.repairing = false;
        }

        if (!creep.memory.repairing && creep.store.getFreeCapacity() == 0) {
            creep.memory.repairing = true;
        }

        if (creep.memory.repairTask.done) {
            assignRepairTask(creep);
        }
    },

    goRepair: function(creep) {
        let repairTask = Game.getObjectById(creep.memory.repairTask.id);

        if (repairTask == null) {
            // repair id no longer valid; removed/destroyed
            creep.memory.repairTask.done = true;
        }

        if (repairTask.structureType == STRUCTURE_RAMPART) {
            if (creep.memory.homeRoom == "E34S38") {
                if (repairTask.hits > 100000) {
                    creep.memory.repairTask.done = true;
                }
            } else {
                if (repairTask.hits > 300000) {
                    creep.memory.repairTask.done = true;
                }
            }
        }

        if (repairTask.structureType == STRUCTURE_WALL) {
            if (repairTask.hits > 100000) {
                creep.memory.repairTask.done = true;
            }
        }

        if (repairTask.hits == repairTask.hitsMax) {
            creep.memory.repairTask.done = true;
        } else {
            if (creep.repair(repairTask) == ERR_NOT_IN_RANGE) {
                creep.moveTo(repairTask);
            }
        }
    },

    goDismantle: function(creep, target) {
        if (creep.dismantle(target) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }
    }
}

function checkRepairList(creep) {
    if (creep.memory.repairTask == undefined) {
        assignRepairTask(creep);
    }
}

function assignRepairTask(creep) {
    let repairJobs = creep.room.memory.repairJobs;

    if (repairJobs.length > 50) {
        console.log("Room: " + creep.room.name + " Repair list seems high: " + repairJobs.length);
    }

    // TODO: Check if repairList[0].repairId still exists (structure removed/destroyed)

    if (repairJobs.length > 0) {
        creep.memory.repairTask = { id: repairJobs[0].repairId, done: false };
        repairJobs.shift();

        if (repairJobs.length > 0) {
            creep.room.memory.repairJobs = repairJobs;
        }
    } else {
        console.log("Engineer in room: " + creep.room.name + " is bored.");
    }
}

module.exports = engineer;
