var collectEnergy = {
    fromContainers: function(creep) {
        var sources = creep.room.find(FIND_STRUCTURES, {
            filter: (s) => {
                return (s.structureType == STRUCTURE_CONTAINER)
                    && s.store.getUsedCapacity(RESOURCE_ENERGY) > 0;
            }
        });

        if (sources.length > 0) {
            var index = creep.memory.source_index;
            var withdrawCode = creep.withdraw(sources[index], RESOURCE_ENERGY);
            if (withdrawCode != ERR_NOT_IN_RANGE && withdrawCode != OK) {
                console.log('collect issue: ' + withdrawCode);
            }
            if (creep.withdraw(sources[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0]);
            } else if (creep.withdraw(sources[index], RESOURCE_ENERGY) == ERR_NOT_ENOUGH_RESOURCES) {
                increaseIndex(creep, sources);
            } else if (creep.withdraw(sources[index], RESOURCE_ENERGY) == ERR_INVALID_TARGET) {
                creep.memory.source_index = 0;
                //increaseIndex(creep, sources);
            }
        }
    },

    fromStorage: function(creep) {
        var storage = creep.room.storage;
        if (storage) {
            if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(storage);
            }
        }
    },

    sourcesFromFloor: function(creep) {
        var sources = creep.room.find(FIND_DROPPED_RESOURCES);
        if (sources.length > 0) {
            return sources;
        }
    },

    fromFloor: function(creep, sources) {
        collectFromFloor(creep, sources);
    },

    tombstoneSources: function(creep) {
        var sources = creep.room.find(FIND_TOMBSTONES, {
            filter: (tombstone) => tombstone.store.getUsedCapacity() > 0
        });
        if (sources.length > 0) {
            return sources;
        }
    },

    fromTombstone: function(creep, source, resourceType) {
        if (creep.withdraw(source, resourceType) == ERR_NOT_IN_RANGE) {
            creep.moveTo(source);
        }
    },

    ruinSources: function(creep) {
        var sources = creep.room.find(FIND_RUINS, {
            filter: (ruin) => ruin.store.getUsedCapacity() > 0
        });
        if (sources.length > 0) {
            return sources;
        }
    },

    fromRuin: function(creep, source, resourceType) {
        if (creep.withdraw(source, resourceType) == ERR_NOT_IN_RANGE) {
            creep.moveTo(source);
        }
    }
}

function collectFromFloor(creep, target) {
    if (creep.pickup(target) == ERR_NOT_IN_RANGE) {
        creep.moveTo(target);
    }
}

function increaseIndex(creep, sources) {
    creep.memory.source_index++;
    if (creep.memory.source_index > sources.length) {
        creep.memory.source_index = 0;
    }
}

module.exports = collectEnergy;
