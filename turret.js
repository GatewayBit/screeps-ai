var attack = require('routine.attack');

var roleTurret = {

    run: function(tower) {
        var attackTargets = attack.findAttackTargets(tower);
        if (attackTargets.length > 0) {
            tower.attack(attackTargets[0]);
        }
    }
}

module.exports = roleTurret;
