var collectRoutine = require('routine.collect.energy');
var builderRoutine = require('routine.builder');
var upgradeRoutine = require('routine.upgrade');

var roleRunner = {
    run: function(creep) {
        if (creep.memory.working && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.working = false;
        }
        if (!creep.memory.working && creep.store.getFreeCapacity() == 0) {
            creep.memory.working = true;
        }

        if (creep.memory.working) {
            var closestExtension = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => {
                    return (s.structureType == STRUCTURE_EXTENSION) && s.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                }
            });
            if (closestExtension) {
                if (creep.transfer(closestExtension, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestExtension);
                }
            } else {
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (s) => {
                        return (s.structureType == STRUCTURE_TOWER ||
                            s.structureType == STRUCTURE_SPAWN) &&
                            s.store.getFreeCapacity(RESOURCE_ENERGY) > 0;
                    }
                });
                if (targets.length > 0) {
                    if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0]);
                    }
                } else {
                    var constructionJobs = builderRoutine.hasConstructionSites(creep);
                    if (constructionJobs) {
                        builderRoutine.goBuild(creep, constructionJobs[0]);
                    } else {
                        upgradeRoutine.goUpgrade(creep);
                    }
                }
            }
        } else {
            // TODO: Watch for bugs in this area when picking up other resources
            if (creep.store.getUsedCapacity() > 0 && creep.store.energy == 0) {
                console.log("Empty runner inventory");
                for (const resourceType in creep.carry) {
                    if (creep.transfer(creep.room.storage, resourceType) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(creep.room.storage);
                    }
                }
            } else {
                const tombstoneSources = collectRoutine.tombstoneSources(creep);
                const ruinSources = collectRoutine.ruinSources(creep);
                if (tombstoneSources) {
                    // Collect all resources in the tombstone
                    for (var resource in tombstoneSources[0].store) {
                        collectRoutine.fromTombstone(creep, tombstoneSources[0], resource);
                    }
                } else if(ruinSources) {
                    for (var resource in ruinSources[0].store) {
                        collectRoutine.fromRuin(creep, ruinSources[0], resource);
                    }
                } else {
                    collectRoutine.fromStorage(creep);
                }
            }
        }
    }
}

module.exports = roleRunner;
