var harvester = {
    roomHasResources: function(creep) {
        var targets = creep.room.find(FIND_SOURCES);
        if (targets.length > 0) {
            //Memory.resources = targets;
            return targets;
        }
    },

    goHarvest: function(creep, target) {
        const harvestResult = creep.harvest(target);
        switch (harvestResult) {
            case OK: // ignore
                break;
            case ERR_NOT_IN_RANGE:
                creep.moveTo(target);
                break;
            case ERR_NOT_ENOUGH_RESOURCES:
                creep.moveTo(target);
                break;
            default:
                console.log("harvest result not handled: " + harvestResult);
        }
    }
}


module.exports = harvester;
