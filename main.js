var roleHarvester = require('role.harvester');
var roleBuilder = require('role.builder');
var roleUpgrader = require('role.upgrader');
var turret = require('turret');
var roleBigHarvester = require('role.big.harvester');
var roleRunner = require('role.runner');
var roleMiner = require('role.miner');
var roleRaider = require('role.raider');
var roleClaimer = require('role.claimer');
var roleEngineer = require('role.engineer');
var roleDefender = require('role.defender');
var roleExtractor = require('role.extractor');
var roleHauler = require('role.hauler');
var roleStationaryHarvester = require('role.stationary.harvester');
var roleCleaner = require('role.cleaner');
var roleMicroUpgrader = require('role.micro.upgrader');

const RUNNER_TOTAL = 2;
const BIG_HARVESTER_TOTAL = 0;
const UPGRADER_TOTAL = 1;
const BUILDER_TOTAL = 2;
const MINER_TOTAL = 0;
const RAIDER_TOTAL = 0;
const CLAIMER_TOTAL = 0;
const ENGINEER_TOTAL = 2; // TODO: Implement dynamic scaling depending on repair demand
const DEFENDER_TOTAL = 0; // TODO: Implement check for multiple enemies in the room and scale
const EXTRACTOR_TOTAL = 0; // TODO: DISABLED UNTIL RUNNER MINERAL DEPOSIT BUG IS FIXED
// TODO: Check if minerals are depleted and disable spawning
const HAULER_TOTAL = 1;
const STATIONARY_HARVESTER_TOTAL = 2;
const CLEANER_TOTAL = 1;

module.exports.loop = function () {


    if (Memory.rooms == undefined) {
        var roomNameHash = {};
        for (let name in Game.rooms) {
            var room = Game.rooms[name];

            if (room.controller.my) {
                roomNameHash[room.name] = room;
            }
        }
        Memory.rooms = roomNameHash;
    }

    for (let name in Memory.rooms) {
        var room = Game.rooms[name];

        if (room.memory.repairJobs == undefined || room.memory.repairJobs <= 0) {
            console.log("Room: " + room.name + " has no repair jobs listed.");
            var targets = room.find(FIND_STRUCTURES, {
                filter: (s) => s.hits < s.hitsMax
            });

            targets.sort((a,b) => a.hits - b.hits);

            var repairJobs = [];
            for (let i = 0; i < targets.length; i++) {
                var entry = {};
                entry.repairId = targets[i].id;

                repairJobs.push(entry);
            }

            room.memory.repairJobs = repairJobs;
        }

        var creepLimit;
        switch (room.name) {
            case "E33S38":
                creepLimit = {
                    RUNNER_TOTAL: 2,
                    UPGRADER_TOTAL: 1,
                    BUILDER_TOTAL: 1,
                    ENGINEER_TOTAL: 2,
                    HAULER_TOTAL: 1,
                    STATIONARY_HARVESTER_TOTAL: 2,
                    CLEANER_TOTAL: 1,
                    MICRO_ATTACKER_TOTAL: 1
                };
                break;
            case "E34S38":
                creepLimit = {
                    HARVESTER_TOTAL: 4,
                    MICRO_UPGRADER_TOTAL: 2,
                    BUILDER_TOTAL: 0,
                    ENGINEER_TOTAL: 1,
                    MICRO_DEFENDER_TOTAL: 1,
                    STATIONARY_HARVESTER_TOTAL: 0
                };
                break;
            default:
                console.log("Implement creep limits for room: " + room.name);
                break;
        }

        room.memory.creepLimit = creepLimit;

        // Count creeps in room
        var creepCount = 0;
        var harvesterCount = 0;
        var microUpgraderCount = 0;
        var microDefenderCount = 0;
        var microAttackerCount = 0;
        var builderCount = 0;
        var engineerCount = 0;
        var stationaryHarvesterCount = 0;
        for (let name in Game.creeps) {
            var creep = Game.creeps[name];
                creepCount++;
                switch (creep.memory.role) {
                    case "harvester":
                        harvesterCount++;
                        break;
                    case "micro_upgrader":
                        microUpgraderCount++;
                        break;
                    case "micro_defender":
                        microDefenderCount++;
                        break;
                    case "micro_attacker":
                        microAttackerCount++;
                        break;
                    case "builder":
                        if (room.name == creep.memory.homeRoom) {
                            builderCount++;
                        }
                        break;
                    case "engineer":
                        if (room.name == creep.memory.homeRoom) {
                            engineerCount++;
                        }
                        break;
                    case "stationary_harvester":
                        if (room.name == creep.memory.homeRoom) {
                            stationaryHarvesterCount++;
                        }
                    default:
                        //console.log("Not tracked: " + creep.memory.role);
                        creepCount++;
                        break;
                }
        }
        //console.log("Room " + room.name + " has " + creepCount + " creeps in it.\n" +
        //    "Harvester: " + harvesterCount + "\n" +
        //    "Micro.Upgrader: " + microUpgraderCount + "\n" +
        //    "Builder: " + builderCount + "\n" +
        //    "Engineer: " + engineerCount);

        if (harvesterCount < room.memory.creepLimit.HARVESTER_TOTAL) {
            console.log("room: " + room.name + " needs harvester");
            testSpawnCreep("harvester", room.name);
        }
        if (microUpgraderCount < room.memory.creepLimit.MICRO_UPGRADER_TOTAL) {
            console.log("room: " + room.name + " needs micro upgrader");
            testSpawnCreep("micro_upgrader", room.name);
        }
        if (microDefenderCount < room.memory.creepLimit.MICRO_DEFENDER_TOTAL) {
            console.log("room: " + room.name + " needs micro defender");
            testSpawnCreep("micro_defender", room.name);
        }

        if (microAttackerCount < room.memory.creepLimit.MICRO_ATTACKER_TOTAL) {
            var attackRoom = "E33S37";
            testSpawnCreep("micro_attacker", attackRoom);
        }

        if (engineerCount < room.memory.creepLimit.ENGINEER_TOTAL) {
            console.log("room: " + room.name + " needs engineer");
            testSpawnCreep("engineer", room.name);
        }

        if (builderCount < room.memory.creepLimit.BUILDER_TOTAL) {
            console.log("room: " + room.name + " needs builder");
            testSpawnCreep("builder", room.name);
        }

        if (stationaryHarvesterCount < room.memory.creepLimit.STATIONARY_HARVESTER_TOTAL) {
            console.log("room: " + room.name + " needs builder");
            testSpawnCreep("stationary_harvester", room.name);
        }
    }



    // For each room
    // Build creep requirement list
    // For each spawn

    var creepCount = 0;
    var creepUpgraderCount = 0;
    var creepBuilderCount = 0;
    var creepBigHarvesterCount = 0;
    var creepRunnerCount = 0;
    var creepMinerCount = 0;
    var creepRaiderCount = 0;
    var creepClaimerCount = 0;
    var creepEngineerCount = 0;
    var creepDefenderCount = 0;
    var creepExtractorCount = 0;
    var creepHaulerCount = 0;
    //var creepStationaryHarvesterCount = 0;
    var creepCleanerCount = 0;

    var creepTestHarvesterCount = 0;
    var creepMicroUpgraderCount = 0;
    var creepMicroAttackerCount = 0;
    var creepMicroDefenderCount = 0;

    // CREEP CODE RUN STAGE
    // CREEP COUNT STAGE
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
            creepTestHarvesterCount++;
        }
        
        if (creep.memory.role == 'builder') {
            roleBuilder.run(creep);
            creepBuilderCount++;
        }

        if (creep.memory.role == 'micro_upgrader') {
            roleMicroUpgrader.run(creep);
            creepMicroUpgraderCount++;
        }

        if (creep.memory.role == 'micro_defender') {
            roleDefender.run(creep);
            creepMicroDefenderCount++;
        }

        if (creep.memory.role == 'micro_attacker') {
            roleDefender.run(creep);
            creepMicroAttackerCount++;
        }
        
        if (creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
            creepUpgraderCount++;
        }

        if (creep.memory.role == 'big_harvester') {
            roleBigHarvester.run(creep);
            creepBigHarvesterCount++;
        }

        if (creep.memory.role == 'runner') {
            roleRunner.run(creep);
            creepRunnerCount++;
        }

        if (creep.memory.role == 'miner') {
            roleMiner.run(creep);
            creepMinerCount++;
        }

        if (creep.memory.role == 'raider') {
            roleRaider.run(creep);
            creepRaiderCount++;
        }

        if (creep.memory.role == 'claimer') {
            roleClaimer.run(creep);
            creepClaimerCount++;
        }

        if (creep.memory.role == 'engineer') {
            roleEngineer.run(creep);
            creepEngineerCount++;
        }
        
        if (creep.memory.role == 'defender') {
            roleDefender.run(creep);
            creepDefenderCount++;
        }

        if (creep.memory.role == 'extractor') {
            roleExtractor.run(creep);
            creepExtractorCount++;
        }

        if (creep.memory.role == 'hauler') {
            roleHauler.run(creep);
            creepHaulerCount++;
        }

        if (creep.memory.role == 'stationary_harvester') {
            roleStationaryHarvester.run(creep);
            //creepStationaryHarvesterCount++;
        }

        if (creep.memory.role == 'cleaner') {
            roleCleaner.run(creep);
            creepCleanerCount++;
        }

        creepCount++;
    }

    // SPAWNING STAGE
    // TODO: Implement better priority spawning
    if (creepCount < 3) {
        spawnCreep('harvester');
    }

    if (creepRunnerCount < RUNNER_TOTAL) {
        spawnCreep('runner');
    } else {
        if (creepBigHarvesterCount < BIG_HARVESTER_TOTAL) {
            spawnCreep('big_harvester');
        }
        
        if (creepUpgraderCount < UPGRADER_TOTAL) {
            spawnCreep('upgrader');
        }
        
       // if (creepBuilderCount < BUILDER_TOTAL) {
       //     spawnCreep('builder');
       // }

        if (creepMinerCount < MINER_TOTAL) {
            spawnCreep('miner');
        }

        if (creepRaiderCount < RAIDER_TOTAL) {
            spawnCreep('raider');
        }

        if (creepClaimerCount < CLAIMER_TOTAL) {
            testSpawnCreep("claimer", "E33S37");
        }

        if (creepEngineerCount < ENGINEER_TOTAL) {
            spawnCreep('engineer');
        }
        
        if (creepDefenderCount < DEFENDER_TOTAL) {
            spawnCreep('defender');
        }

        if (creepExtractorCount < EXTRACTOR_TOTAL) {
            spawnCreep('extractor');
        }

        if (creepHaulerCount < HAULER_TOTAL) {
            spawnCreep('hauler');
        }

        //if (creepStationaryHarvesterCount < STATIONARY_HARVESTER_TOTAL) {
        //    spawnCreep('stationary_harvester');
        //}

        if (creepCleanerCount < CLEANER_TOTAL) {
            spawnCreep('cleaner');

        }
    }

    cleanMemory();

    for (var i in Game.structures) {
        var tower = Game.structures[i];
        if (tower.structureType == STRUCTURE_TOWER) {
            turret.run(tower);
        }
    }

    processLinkStructures();
}


function processLinkStructures() {
    const linkFrom = Game.getObjectById("5de079267109a94cf69cf48b");
    const linkTo = Game.getObjectById("5ddefe40f18cf199b99102dc");

    if (linkTo.store[RESOURCE_ENERGY] < 700) {
        linkFrom.transferEnergy(linkTo);
    }
}

function testSpawnCreep(roleType, room) {
    var creep = buildCreep(roleType, room);
    protoSpawnCreep(creep);
}

function buildCreep(roleType, room) {
    var creep = {};
    creep.body = buildBody(buildBodyRequest(roleType));
    creep.name = roleType + Game.time;
    creep.memory = {role: roleType, homeRoom: room};
    return creep;
}

function buildBodyRequest(roleType) {
    var bodyRequest;
    switch(roleType) {
        case "harvester":
            bodyRequest = [
                {part: MOVE, count: 4},
                {part: WORK, count: 2},
                {part: CARRY, count: 6}
            ];
            break;
        case "claimer":
            bodyRequest = [
                {part: MOVE, count: 4},
                {part: CLAIM, count: 8}
            ];
            break;
        case "micro_upgrader":
            bodyRequest = [
                {part: WORK, count: 3},
                {part: CARRY, count: 2},
                {part: MOVE, count: 3}
            ];
            break;
        case "micro_defender":
            bodyRequest = [
                {part: TOUGH, count: 5},
                {part: MOVE, count: 5},
                {part: ATTACK, count: 4}
            ];
            break;
        case "micro_attacker":
            bodyRequest = [
                {part: TOUGH, count: 5},
                {part: MOVE, count: 5},
                {part: ATTACK, count: 4}
            ];
            break;
        case "engineer":
            bodyRequest = [
                {part: WORK, count: 2},
                {part: CARRY, count: 4},
                {part: MOVE, count: 4}
            ];
            break;
        case "builder":
            bodyRequest = [
                {part: MOVE, count: 6},
                {part: WORK, count: 6},
                {part: CARRY, count: 6}
            ];
            break;
        case "stationary_harvester":
            bodyRequest = [
                {part: MOVE, count: 4},
                {part: WORK, count: 7}
            ];
            break;
        default:
            console.log("No body template set for: " + roleType);
    }
    return bodyRequest;
}

function buildBody(bodyRequest) {
    var body = [];

    for (var i in bodyRequest) {
        var part = bodyRequest[i].part;
        var count = bodyRequest[i].count;
        for (var j = 0; j < count; j++) {
            body.push(part);
        }
    }

    return body;
}

function protoSpawnCreep(creep) {

    var spawnBackup;

    // NOTE: spawns are checked in order and thats why the creepNeedsSpawn thingy isnt working correctly
    // E33S38 s1
    // E33S38 s2
    // E34S38 s1
    for (var name in Game.spawns) {
        var spawn = Game.spawns[name];

        var spawnTestResult = spawn.spawnCreep(creep.body, creep.name, {memory:
            {role: creep.memory.role, homeRoom: creep.memory.homeRoom},
            dryRun: true});

        switch (spawnTestResult) {
            case OK:
                if (spawn.room.name == creep.memory.homeRoom) {
                    spawn.spawnCreep(creep.body, creep.name, {memory:
                        {role: creep.memory.role, homeRoom: creep.memory.homeRoom}
                    });
                } else {
                    spawnBackup = spawn;
                }
                break;
            case ERR_BUSY:
            case ERR_NOT_ENOUGH_ENERGY:
                if (spawnBackup) {
                    spawnBackup.spawnCreep(creep.body, creep.name, {memory:
                        {role: creep.memory.role, homeRoom: creep.memory.homeRoom}
                    });
                }
                break;
            default:
                console.log("Not handled.");
                break;
        }


        // Test if home room can spawn creep
        if (spawn.room.name == creep.memory.homeRoom) {
        }
    }
}

function spawnCreep(roleType) {
    for (let i in Game.spawns) {
        var spawn = Game.spawns[i];
        if (roleType === 'big_harvester') {
            // Resrouce Cost: 800 -- Better value with roads
            // TODO: Convert to container miner?
            spawn.spawnCreep([MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK,CARRY],
                'big.harvester'+Game.time, {memory: {role: roleType}});
        } else if (roleType === 'runner') {
            // Resource Cost: 650
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
                'runner'+Game.time, {memory: {role: roleType, source_index: 0}});
        } else if (roleType === 'upgrader') {
            // Resource cost: 600
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
                'upgrader'+Game.time, {memory: {role: roleType}});
            //spawn.spawnCreep([WORK, WORK, CARRY, MOVE, CARRY, MOVE, CARRY, MOVE, CARRY, MOVE],
            //    'upgrader'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'builder') {
            console.log("TODO: REMOVE legacy stuff");
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY], 'builder'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'miner') {
            // TODO: Convert to container miner?
            // Resource cost: 1000
            spawn.spawnCreep([WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE], 'miner'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'raider') {
            // Resource cost: 350
            spawn.spawnCreep([WORK, CARRY, CARRY, MOVE, MOVE, MOVE], 'raider'+Game.time, {memory: {role: roleType, inHome: true}});
        } else if (roleType == 'claimer') {
            spawn.spawnCreep([CLAIM, MOVE], 'claimer'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'engineer') {
            spawn.spawnCreep([WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], 'engineer'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'defender') {
            spawn.spawnCreep([TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK,ATTACK], 'defender'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'extractor') {
            spawn.spawnCreep([MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK], 'extractor'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'hauler') {
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,MOVE,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY], 'hauler'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'stationary_harvester') {
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK],
                'stationary_harvester'+Game.time, {memory: {role: roleType}});
        } else if (roleType == 'cleaner') {
            spawn.spawnCreep([MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
                'cleaner'+Game.time, {memory: {role: roleType}});
        } else {
            // Resource cost: 200
            spawn.spawnCreep([WORK,CARRY,MOVE],
                'worker'+Game.time, {memory: {role: roleType}});
        }
    }
}

function cleanMemory() {
    for (var name in Memory.creeps) {
        if (!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log("Removing memory of: " + name);
        }
    }
}
