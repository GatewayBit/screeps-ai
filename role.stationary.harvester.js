var roleStationaryHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name == creep.memory.homeRoom) {
            checkSourceList(creep);

            var source = Game.getObjectById(creep.memory.claimedSource.id);

            var sources = creep.room.find(FIND_SOURCES);
            var containers = creep.room.find(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
            });

            if (creep.memory.harvestPos == undefined) {
                for (let i = 0; i < containers.length; i++) {
                    if (source.pos.isNearTo(containers[i].pos)) {
                        console.log("Source id: " + source.id + " is next to container id: " + containers[i].id);
                        creep.memory.harvestPos = containers[i].pos;
                        break;
                    }
                }
            } else {
                var harvestPos = creep.memory.harvestPos;
                if (creep.pos.isEqualTo(harvestPos.x, harvestPos.y)) {
                    creep.harvest(source);
                } else {
                    creep.moveTo(harvestPos.x, harvestPos.y);
                }
            }
        } else {
        const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
        const exit = creep.pos.findClosestByRange(exitDir);
        creep.moveTo(exit);
        }
    }
};

function checkSourceList(creep) {
    if (Memory.sourceList == undefined) {
        initSourceList(creep);
    } else {
        if (creep.memory.claimedSource == undefined) {
            assignEnergySource(creep);
        }
    }
}
function assignEnergySource(creep) {
    // What if null?
    let sourceList = Memory.sourceList;

    creep.memory.claimedSource = { id: sourceList[0].sourceId };
    sourceList.shift();

    if (sourceList.length > 0) {
        Memory.sourceList = sourceList;
    } else {
        initSourceList(creep);
    }

}

function initSourceList(creep) {
    console.log("Building source list for room");
    var sources = creep.room.find(FIND_SOURCES);

    var sourceList = [];
    for (let i = 0; i < sources.length; i++) {
        var entry = {};
        entry.sourceId = sources[i].id;

        sourceList.push(entry);
    }

    Memory.sourceList = sourceList;
}

module.exports = roleStationaryHarvester;
