var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if (creep.room.name == creep.memory.homeRoom) {
            if (creep.memory.building && creep.store[RESOURCE_ENERGY] == 0) {
                creep.memory.building = false;
            }

            if (!creep.memory.building && creep.store.getFreeCapacity() == 0) {
                creep.memory.building = true;
            }

            if(!creep.memory.building) {

                var tombstones = creep.room.find(FIND_TOMBSTONES, {
                    filter: (t) => t.store.getUsedCapacity() > 100
                });
                if (tombstones.length > 0) {
                    if (creep.withdraw(tombstones[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(tombstones[0]);
                    }
                } else {
                    var sources = creep.room.find(FIND_SOURCES);
                    if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(sources[0]);
                    }
                }
            } else {
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (s) => {
                        return (s.structureType == STRUCTURE_EXTENSION ||
                            s.structureType == STRUCTURE_SPAWN ||
                            s.structureType == STRUCTURE_CONTAINER ||
                            s.structureType == STRUCTURE_TOWER) &&
                            (s.store.getFreeCapacity(RESOURCE_ENERGY) > 0)
                    }
                });
                if (targets.length > 0) {
                    var targetStructure;
                    for (let i in targets) {
                        var target = targets[i];
                        var structureType = target.structureType;
                        switch (structureType) {
                            case STRUCTURE_EXTENSION:
                                // ** INTENTIONAL FALLTHROUGH **
                            case STRUCTURE_SPAWN:
                                targetStructure = target;
                                break;
                            case STRUCTURE_CONTAINER:
                                targetStructure = target;
                                break;
                            case STRUCTURE_TOWER:
                                targetStructure = target;
                                break;
                            default:
                                console.log("Implement structure: " + structureType);
                                break;
                        }
                    }
                    if (creep.transfer(targetStructure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targetStructure);
                    }
                } else {
                    if (creep.room.storage) {
                        if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(creep.room.storage);
                        }
                    } else {
                        var target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);
                        if (target) {
                            if (creep.build(target) == ERR_NOT_IN_RANGE) {
                                creep.moveTo(target);
                            }
                        }
                    }
                }
            }
        } else {
            const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
            const exit = creep.pos.findClosestByRange(exitDir);
            creep.moveTo(exit);
        }
	}
};

module.exports = roleHarvester;
