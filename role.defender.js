var attack = require('routine.attack');

var roleDefender = {
    run: function(creep) {
        if (creep.room.name == creep.memory.homeRoom) {
            if (creep.memory.attack) {
                var targets = creep.room.find(FIND_HOSTILE_SPAWNS);
                if (targets.length > 0) {
                    if (creep.attack(targets[0]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0]);
                    }
                } else {
                    var targets = attack.findAttackTargets(creep);
                    if (targets.length > 0) {
                        if (creep.attack(targets[0]) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(targets[0]);
                        }
                    } else {
                        creep.moveTo(new RoomPosition(40, 30, creep.memory.homeRoom));
                    }
                }
            } else {
                var targets = attack.findAttackTargets(creep);
                if (targets.length > 0) {
                    if(creep.attack(targets[0]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0]);
                    }
                } else {
                    creep.moveTo(new RoomPosition(30,38, creep.memory.homeRoom));
                }
            }
        } else {
            const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
            const exit = creep.pos.findClosestByRange(exitDir);
            creep.moveTo(exit);
        }
    }
}

module.exports = roleDefender;
