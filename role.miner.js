var harvester = require('routine.harvester');

var roleMiner = {
    run: function(creep) {
        if (creep.store.getFreeCapacity() > 0) {
            const targets = harvester.roomHasResources(creep);

            harvester.goHarvest(creep, targets[1]);
        } else {
            if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.storage);
            }
        }
    }
}

module.exports = roleMiner;
