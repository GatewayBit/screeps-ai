var roleClaimer = {
    run: function(creep) {
        if (creep.room.name == creep.memory.homeRoom) {
            if (creep.room.controller && !creep.room.controller.my) {
                console.log("claim: " + creep.claimController(creep.room.controller));
                var claimResult = creep.claimController(creep.room.controller);
                switch (claimResult) {
                    case OK:
                        console.log("Winrar");
                        break;
                    case ERR_INVALID_TARGET:
                        var attackResult = creep.attackController(creep.room.controller);
                        console.log(attackResult);
                        if (attackResult == ERR_NOT_IN_RANGE) {
                            creep.moveTo(creep.room.controller);
                        }
                        break;
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(creep.room.controller);
                    default:
                        break;
                }
            }
        } else {
            const exitDir = creep.room.findExitTo(creep.memory.homeRoom);
            const exit = creep.pos.findClosestByRange(exitDir);
            creep.moveTo(exit);
        }
    }
}

module.exports = roleClaimer;
