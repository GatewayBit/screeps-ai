var collectRoutine = require('routine.collect.energy')
var roleUpgrader = {
    run: function(creep) {
        if (creep.memory.upgrading && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.upgrading = false;
        }
        
        if (!creep.memory.upgrading && creep.store.getFreeCapacity() == 0) {
            creep.memory.upgrading = true;
        }
        
        if (creep.memory.upgrading) {
            if (creep.room.controller) {
                if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller);
                }
            }
        } else {
            const upgradeArea = new RoomPosition(39,11, 'E33S38');
            if (creep.pos.inRangeTo(upgradeArea, 3)) {
                // TODO: Allow upgraders to pull from containers or room.storage
                const container = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (s) => (s.structureType == STRUCTURE_CONTAINER ||
                        s.structureType == STRUCTURE_LINK)
                    && s.store.getUsedCapacity(RESOURCE_ENERGY) > 0
                });

                console.log(container);

                if (creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(container);
                }
            } else {
                creep.moveTo(upgradeArea);
            }
        }
    }
};

module.exports = roleUpgrader;
