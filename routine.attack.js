var attack = {
    findAttackTargets: function(entity) {
        const targets = entity.room.find(FIND_HOSTILE_CREEPS);
        return targets;
    }
}

module.exports = attack;
