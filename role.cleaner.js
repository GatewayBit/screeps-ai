var roleCleaner = {
    run: function(creep) {
        if (creep.memory.working && creep.store[RESOURCE_ENERGY] == 0) {
            creep.memory.working = false;
        }
        if (!creep.memory.working && creep.store.getFreeCapacity() == 0) {
            creep.memory.working = true;
        }

        if (creep.memory.working) {
            var storage = creep.room.storage;
            if (creep.transfer(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(storage);
            }
        } else {
            checkContainerList(creep);

            var container = Game.getObjectById(creep.memory.currentContainer.id);

            var withdrawResult = creep.withdraw(container, RESOURCE_ENERGY);
            switch (withdrawResult) {
                case OK:
                    assignContainer(creep);
                    break;
                case ERR_BUSY:
                    // Ignore spawning code value for now
                    break;
                case ERR_NOT_ENOUGH_RESOURCES:
                    assignContainer(creep);
                    break;
                case ERR_NOT_IN_RANGE:
                    creep.moveTo(container);
                    break;
                default:
                    console.log("cleaner withdraw not handled: " + withdrawResult);
                    break;
            }
        }
    }
};

function checkContainerList(creep) {
    if (creep.memory.containerList == undefined) {
        initContainerList(creep);
    } else {
        if (creep.memory.currentContainer == undefined) {
            assignContainer(creep);
        }
    }
}

function assignContainer(creep) {
    let containerList = creep.memory.containerList;
    creep.memory.currentContainer = { id: containerList[0].containerId };
    containerList.shift();

    if (containerList.length > 0) {
        creep.memory.containerList = containerList;
    } else {
        initContainerList(creep);
    }
}

function initContainerList(creep) {
    // TODO: Remove hard coded list
    var containers = [
        "5de2b6f0154f54669ed35ff6",
        "5de298ed68c96c206e90d57a"
    ];

    var containerList = [];
    for (let i = 0; i < containers.length; i++) {
        var entry = {};
        entry.containerId = containers[i];

        containerList.push(entry);
    }

    creep.memory.containerList = containerList;
}

 module.exports = roleCleaner;
