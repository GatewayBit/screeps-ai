var roleExtractor = {
    run: function(creep) {

        var minerals = creep.room.find(FIND_MINERALS, {
            filter: (m) => m.mineralType == RESOURCE_KEANIUM
        });

        if (creep.harvest(minerals[0]) == ERR_NOT_IN_RANGE) {
            creep.moveTo(minerals[0]);
        }
	}
};

module.exports = roleExtractor;
