var roleBigHarvester = {
    run: function(creep) {
        if (creep.store.getFreeCapacity() > 0) {
            var sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(33,20);
            }

            pickupEnergy(creep);

        } else {
            var targets = creep.pos.findInRange(FIND_STRUCTURES, 2, {
                filter: (s) => (s.structureType == STRUCTURE_CONTAINER) && s.store.getFreeCapacity() > 0
            });

            if (targets.length > 0) {
                if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(33,20);
                }
            }
        }
    }
}

/**
 * Pickup excess energy harvested off the floor
 */
function pickupEnergy(creep) {
    var droppedEnergy = creep.pos.lookFor(LOOK_ENERGY);
    if (droppedEnergy != undefined && droppedEnergy.length > 0) {
        creep.pickup(droppedEnergy[0]);
    }
}

module.exports = roleBigHarvester;
